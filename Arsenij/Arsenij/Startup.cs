﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Arsenij.Startup))]
namespace Arsenij
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
